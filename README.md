# README #
Fork of webvim https://github.com/vim-dist/webvim

## Changes ##

* use atom-dark theme
* used luna airline theme

* webvim https://github.com/vim-dist/webvim
* theme picker https://github.com/vim-airline/vim-airline-themes
* atom theme https://github.com/rakr/vim-one
* vi cheatsheet http://fprintf.net/vimCheatSheet.html

## dependancies ##

```sh
apt-get install vim vim-runtime vim-gui-common

apt-get install build-essential cmake python-dev exuberant-ctags

sudo npm cache clean -f
sudo npm install -g n
sudo n stable
sudo ln -sf /usr/local/n/versions/node/<VERSION>/bin/node /usr/bin/nodejs

npm install -g eslint csslint jshint jsonlint handlebars
```


### instal from orginal repo ###
```sh
git clone https://github.com/krampstudio/webvim.git ~/.vim
ln -s ~/.vim/.vimrc ~/.vimrc
ln -s ~/.vim/.tern-project ~/.tern-project
vim
```

## Hardcore mode

The hardcore mode remap some keys to force you use Vim in a productive way:
 - no arrow keys for moving instead use the common Vim movement commands.
 - in insert mode, use `jk` to switch back to normal mode instead of `<esc>`

Restricting commands is the best way to make your fingers learn. After one or two days, you should be more productive and have learned lots of Vim commands.

You can disable the hardcore mode by setting the value of `g:hardcoreMode` to `0` in `.vimrc`. You can also change the mappings.

## Usage

### vim things to remember ###

:set runtimepath?  shows paths involved

:AirlinTheme lune  set airline theme to luna

:color one  set color theme to one dark

AirlineTheme base16
call AirlineTheme base16
set AirlineTheme base16


### Vim

WebVim is only a Vim distribution with plugins and configuration, so all common Vim commands and basic mapping are available. You must know how to use Vim before using this IDE. If you're not comfortable with Vim enough I suggest you to take the interactive Vim tutorial (run `vimtutor` in a terminal), and keep a [common usage cheat sheet](http://fprintf.net/vimCheatSheet.html) close to you until your fingers get all the mappings in memory.

### WebVim

|                                                  | Command               | Mode | Context          |
|--------------------------------------------------|-----------------------|:----:|------------------|
| __Plugins__                                                                                        |
| Install Plugins                                  | `:PlugInstall`        | n    |                  |
| Update Plugins                                   | `:PlugUpdate`         | n    |                  |
|                                                                                                    |
| __Config__                                                                                         |
| Edit .vimrc                                      | `<leader>ev`           | n    |                  |
| Reload .vimrc                                    | `<leader>sv`           | n    |                  |
|                                                                                                    |
| __File Tree (NERDTree)__                                                                           |
| Toggle Tree                                      | `<c-n>`               | n    |                  |
| Open a node in a new tab                         | `t`                   |      | Tree Node        |
| Change Root                                      | `C`                   |      | Tree Node        |
| Tree menu                                        | `m`                   |      | Tree Node        |
| Add a file                                       | `a`                   |      | Tree Menu        |
| Delete a file                                    | `d`                   |      | Tree Menu        |
| Move a file                                      | `m`                   |      | Tree Menu        |
| Copy a file                                      | `c`                   |      | Tree Menu        |
| Search in files (grep)                           | `g`                   |      | Tree Menu        |
| Next match in grep                               | `:cn`                 |      | Grep > Quickfix  |
| Move to left tab                                 | `<s-left>`            | n    |                  |
| Move to right tab                                | `<s-right>`           | n    |                  |
| Change window (ie. tree to tab)                  | `<c-w><c-w>`          |      |                  |
| Help                                             | `?`                   |      | Tree             |
| Documentation                                    | `:help NERDTree`      |      |                  |
|                                                                                                    |
| __Comment__                                                                                        |
| Toggle comments                                  | `<c-/>`               | nv   |                  |
| Comments                                         | `<leader>cc`          | nv   |                  |
| Sexy Comments                                    | `<leader>cs`          | nv   |                  |
| UnComments                                       | `<leader>cu`          | nv   |                  |
| Yank and Comments                                | `<leader>cy`          | nv   |                  |
| Documentation                                    | `:help NERDCommenter` |      |                  |
|                                                                                                    |
| __Align__                                                                                          |
| Start interactive alignment                      | `EasyAlign`           | v    | selection        |
| Align next paragraph on =                        | `<leader>a=`          | n    |                  |
| Align next paragraph on :                        | `<leader>a:`          | n    |                  |
| Align next paragraph on delimiter _x_            | `<leader>ax`          | n    |                  |
| Right align selection on =                       | `<leader>a=`          | v    |                  |
| Right align selection on :                       | `<leader>a:`          | v    |                  |
| Right align selection on _x_                     | `<leader>ax`          | v    |                  |
| Documentation                                    | `:help :EasyAlign`    |      |                  |
|                                                                                                    |
| __Format__                                                                                         |
| Format the file                                  | `<c-f>`               | n    | js,json,html,css |
| Format the selection                             | `<c-f>`               | n    | js,json,html,css |
|                                                                                                    |
| __Multiple Cursor__                                                                                |
| Start multiple cursor                            | `<c-m>`               | v    | Visual Block     |
| Multiple cursor insert                           | `i`                   |      | multiple cursor  |
| Multiple cursor remove                           | `x`                   |      | multiple cursor  |
| Leave multiple cursor                            | `<esc>`               |      | multiple cursor  |
|                                                                                                    |
| __Paste__                                                                                          |
| cycle backward through your history of yanks     | `<leader>p`           | nv   | after paste `p`  |
| cycle forward through your history of yanks      | `<leader>P`           | nv   | after paste `p`  |
|                                                                                                    |
| __AutoCompletion__                                                                                 |
| Select next proposal in menu                     | `<tab>`               | i    | complete menu    |
| Select previous proposal in menu                 | `<shift><tab>`        | i    | complete menu    |
|                                                                                                    |
| __Syntax checking__                                                                                |
| Checkers infos                                   | `:SyntasticInfo`      | n    |                  |
| Check                                            | `:SyntasticCheck`     | n    |                  |
| Toggle check                                     | `:SyntasticToggleMode`| n    |                  |
| Error window                                     | `:Errors`             | n    |                  |
| Jump next error                                  | `:lnext`              | n    |                  |
| Jump previous error                              | `:lprev`              | n    |                  |
|                                                                                                    |
| __JavaScript__                                                                                     |
| Get type                                         | `<leader>gt`          | n    | under cursor     |
| Get documentation                                | `<leader>gd`          | n    | under cursor     |
| Go to                                            | `<leader>go`          | n    | under cursor     |
| Jump to definition                               | `<leader>gf`          | n    | under cursor     |
| Go to references                                 | `<leader>gr`          | n    | under cursor     |
| Rename                                           | `<leader>r`           | n    | under cursor     |
| Jump to the source of a `require`                | `gf`                  | n    | node.js, cursor  |
| Edit the main file of a CJS module               | `:Nedit module`       | n    | node.js          |
| Edit a file of a CJS module                      | `:Nedit module/foo.js`| n    | node.js          |
| Edit projects main (from package.json)           | `:Nedit`              | n    | node.js          |
|                                                                                                    |
| __Git__                                                                                            |
| git diff                                         | `:Gdiff`              | n    |                  |
| git status                                       | `:Gstatus`            | n    |                  |
| git commit                                       | `:Gcommit`            | n    |                  |
| git blame                                        | `:Gblame`             | n    |                  |
| git mv                                           | `:Gmove`              | n    |                  |
| git rm                                           | `:Gremove`            | n    |                  |
| Open the current file in Github                  | `:Gbrowse`            | n    |                  |
|                                                                                                    |
| __Spell Check__                                                                                    |
| Enable checking                                  | `set spell`            | n   |                  |
| move to the next misspelled word                 | `]s`                   | n   |                  |
| move to the previous misspelled word             | `[s`                   | n   |                  |
| add a word to the dictionary                     | `zg`                   | n   |                  |
| undo the addition of a word to the dictionary    | `zug`                  | n   |                  |
| view spelling suggestions for a misspelled word  | `z=`                   | n   |                  |
|                                                                                                    |
| __Search__                                                                                         |
| clear highlights                                 | `<c-l>`               | n    |                  |
|                                                                                                    |
| __Editing__                                                                                        |
| Move line up                                     | `<leader>-`           | nv   |                  |
| Move line down                                   | `<leader>_`           | nv   |                  |
| Wrap in single quote                             | `<leader>'`           | nv   |                  |
| Wrap in double quote                             | `<leader>"`           | nv   |                  |
|                                                                                                    |
| __Emmet__                                                                                          |
| Expand abbreviation                              | `kj`                  | i    | html,css,scss    |
|                                                                                                    |
| _Next sections to come soon_                                                                       |
|                                                                                                    |

_Modes_ :
 - `n` normal
 - `i` insert
 - `v` visual

_Commands_ :
 - `:command` a Vim command
 - `:set something` can also be replaced by `:setlocal something` to apply it to the current buffer only
 - `<c-m>a` or `a` a keyboard command
   - `<c-/>` means `CTRL and `/` (this is the Vim notation)
   - `<s-left>` means `Shift` and `left arrow`
   - `<c-a>b` means `CTRL` and `a`, then `b`
   - `<leader>` is mapped to `,`
   - `<localleader>` is mapped to `\`
